import sqlite3
from fugashi import Tagger
from pitch import pitchcolorize
from pitch import color_decide
from pitch import color_term
from pitch import color_ass

blacklist_type = ["助詞", "助動詞", "代名詞", "終助詞", "記号", "空白", "固有名詞", "感動詞", "数詞"]
blacklist_words = ["名前"]
def parse_and_blacklist_text(text):
    wordlist = []
    tagger = Tagger()
    for word in tagger(text):
        blacklist_count = False
        for x in blacklist_type:
            if x in word.pos:
                blacklist_count = True
        for y in blacklist_words:
            if word.feature.lemma == None:
                blacklist_count = True
            elif y in word.feature.lemma:
                blacklist_count = True
        if blacklist_count == False:
            wordlist.append(word.feature.lemma)
    return wordlist
def search_dict(input_dict, word):
    conn = sqlite3.connect('/home/stegatxins0/zero/dev/mard0/v1/' + 'dict/dict.db')
    c = conn.cursor()
    select_dict = "SELECT * FROM '" + input_dict + "' where term='" + word + "'"
    c.execute(select_dict)
    res = c.fetchone()
    return res
def output_mpv(res, ind):
    def_count = -1
    def_list = []
    color = color_decide(ind)
    if res == None:
        pass
    else:
        for var in res:
            def_count = def_count + 1
            if var == None:
                break
            if def_count > 7:
                def_list.append(var)
        def_join = ", ".join((def_list))
        return (pitchcolorize(res[1], ind)[0] + color.default + " [" + pitchcolorize(res[1], ind)[1] + color.default + "] " + def_join)
def all_process(inputsentence, dict, out):
    finallist = []
    wordlist = parse_and_blacklist_text(inputsentence)
    for word in wordlist:
        res = search_dict(dict, word)
        finallist.append(output_mpv(res, out))
    return finallist
