import os, json
import pandas as pd
import sqlite3

dictdb = "/home/stegatxins0/zero/dev/mard0/v1/" + "dict/dict.db"
def load_json_files(path_to_json):
    json_files = [pos_json for pos_json in os.listdir(path_to_json) if pos_json.startswith('term_bank')]
    json_full_list = []
    for dictfile in json_files:
        with open(path_to_json + dictfile) as dictfile_json:
            dictfile_json = json.load(dictfile_json)
        for var in dictfile_json:
            json_full_list.append(var)
    return json_full_list

def flatten_def_v1(json_list):
    for word_list in json_list:
        for definition in word_list[5]:
            word_list.append(definition)
        del word_list[5]
    return json_list

def add_index_to_database(path_to_json):
    conn = sqlite3.connect(dictdb)
    c = conn.cursor()
    with open(path_to_json + 'index.json') as index:
        index = json.load(index)
    select_dict = "SELECT * FROM 'dictlist'"
#   close c connection
    while True:
        try:
            c.execute(select_dict)
            index_no = len(c.fetchall()) + 1
            break
        except sqlite3.OperationalError:
            index_no = 1
            break
    index_df = pd.DataFrame(index, index=[index_no])
    index_df.to_sql("dictlist",conn)
    return index['title']

def add_to_database(json_list, title):
    conn = sqlite3.connect(dictdb)
    c = conn.cursor()
    ### Fix to support rows that don't have these number of columns
    df = pd.DataFrame(data=json_list, columns=['term', 'reading', 'tags', 'rule', 'popular', 'expression', 'ttag', 'definition1', 'definition2', 'definition3', 'definition4', 'definition5', 'definition6', 'definition7','definition8', 'definition9', 'definition10', 'definition11', 'definition12', 'definition13'])
    df.sort_values('popular', ascending=False, inplace=True)
    df.reset_index(drop=True)
    df.to_sql(title,conn)
def importdict(path_to_json):
    json_list = load_json_files(path_to_json)
    json_list = flatten_def_v1(json_list)
    title = add_index_to_database(path_to_json)
    add_to_database(json_list, title)
