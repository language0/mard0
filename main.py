from dict import all_process
import sys
import getopt


def main():
    dictionary = 'JMDict (English)'
    output = 'term'

    argv = sys.argv[1:]

    try:
        opts, args = getopt.getopt(argv, "h:i:d:o:p:")

    except:
        print("Error")

    for opt, arg in opts:
        if opt in ['-h']:
            print('mard0 <options> -p <sentence>')
            sys.exit()
        elif opt in ['-i']:
            importdict(arg)
        elif opt in ['-d']:
            dictionary = arg
        elif opt in ['-o']:
            output = arg
        elif opt in ['-p']:
            res = all_process(arg, dictionary, output)
            if res == None:
                pass
            else:
                for finalword in res:
                    if finalword == None:
                        pass
                    else:
                        print(finalword)
main()
