import sqlite3
class color_term:
    heiban = '\033[38;5;33m'
    odaka = '\033[38;5;46m'
    atamadaka = '\033[38;5;196m'
    nakadaka = '\033[38;5;208m'
    kifuku = '\033[38;5;128m'
    default = '\033[0m'
    bold = '\033[1m'
class color_ass:
    heiban = '{\\c&He1872b&}'
    odaka = '{\\c&H7bff08&}'
    atamadaka = '{\\c&H0000f5&}'
    nakadaka = '{\\c&H0000f5&}'
    kifuku = '{\\c&Hff4fc4&}'
    default = '{\\r}'
    bold = '{\\b1}'
conn = sqlite3.connect("/home/stegatxins0/zero/dev/mard0/v1/" + 'dict/pitch.db')
c = conn.cursor()

def color_decide(output):
    if output == 'ass':
        icolor = color_ass
    elif output == 'term':
        icolor = color_term
    return icolor

def pitchcolorize(term, output):
    select_dict = "SELECT * FROM '1' where term='" + term + "'"
    c.execute(select_dict)
    res = c.fetchone()
    onlypitch_ori = res[2].split(' ')
    final_string_term_list = ''
    final_string_furi_list = ''
    for one_string in onlypitch_ori:
        value = one_string[one_string.find("[") : one_string.find("]")+1]
        while True:
            try:
                color = color_decide(output)
                pitch = value[value.find(";")+1]
                if pitch == 'h': tcolor = color.heiban
                elif pitch == 'a': tcolor = color.atamadaka
                elif pitch == 'o': tcolor = color.odaka
                elif pitch == 'n': tcolor = color.nakadaka
                elif pitch == 'k': tcolor = color.kifuku
                else: tcolor = False
                break
            except IndexError:
                tcolor = False
                break
        if tcolor == False:
            final_string_term = color.bold + one_string
            final_string_furi = one_string
        else:
            final_string_term = tcolor + color.bold + one_string[ : one_string.find("[")] + one_string[one_string.find("]") +1 : ]
            final_string_furi = tcolor + one_string[one_string.find("[")+1 : one_string.find(";")]
        final_string_term_list = "".join(final_string_term_list + final_string_term)
        final_string_furi_list = "".join(final_string_furi_list + final_string_furi)
    return final_string_term_list, final_string_furi_list
